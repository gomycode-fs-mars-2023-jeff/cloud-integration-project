import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { addStudent } from '../reduxs/students/StudentsSlice';

const StudentsForm = () => {
    const dispatch = useDispatch();
    const [nom, setNom] = useState('');
    const [prenom, setPrenom] = useState('');
    const [matricule, setMatricule] = useState('');
    const [sexe, setSexe] = useState('');
    const [age, setAge] = useState('');

    const handleSubmit = (e) => {
        e.preventDefault();
        const newStudent = {
            nom: nom,
            prenom: prenom,
            matricule: matricule,
            sexe: sexe,
            age: age,
        };
        
        dispatch(addStudent(newStudent));
        
        setNom('');
        setPrenom('');
        setMatricule('');
        setSexe('');
        setAge('');
    };

    return (
        <div>
            <div className="form-container">
                <div className="title">Ajouter un élève</div>
                <form onSubmit={handleSubmit}>
                    <div className='students-details'>
                        <div className="input-box">
                            <span className='details'>Nom</span>
                            <input placeholder='Entrez nom' type="text" id="nom" required value={nom} onChange={(e) => setNom(e.target.value)} />
                        </div>
                        <div className='input-box'>
                            <span className='details'>Prenom</span>
                            <input placeholder='Entrez prenom' type="text" id="prenom" required value={prenom} onChange={(e) => setPrenom(e.target.value)} />
                        </div>
                        <div className='input-box'>
                            <span className='details'>Matricule</span>
                            <input placeholder='Entrez matricule' type="text" id="matricule" required value={matricule} onChange={(e) => setMatricule(e.target.value)} />
                        </div>
                        <div className='input-box'>
                            <span className='details'>Sexe</span>
                            <select placeholder='Sexe' type="text" id="sexe" required value={sexe} onChange={(e) => setSexe(e.target.value)}>
                                <option value="">Selectionnez sexe</option>
                                <option value="Homme">Homme</option>
                                <option value="Femme">Femme</option>
                            </select>
                        </div>
                        <div className='input-box'>
                            <span className="details">Age</span>
                            <input placeholder='Age' type="number" id="age" required value={age} onChange={(e) => setAge(e.target.value)} />
                        </div>
                    </div>
                    <div className="btn">
                        <button type="submit">Ajouter</button>
                    </div>
                </form>
            </div>
        </div>
    );
}

export default StudentsForm;
