import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import DeleteBtn from '../components/DeleteBtn';
import ModifModal from '../components/ModifModal';


const StudentsList = () => {
    const students = useSelector(state => state.student);

    const [selectedStudent, setSelectedStudent] = useState(null);

  const handleEditClick = (student) => {
    setSelectedStudent(student);
  };

    return (
        <div>
            <h2>Liste des élèves</h2>
            <table className='table-content'>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nom</th>
                        <th>Prénom</th>
                        <th>Matricule</th>
                        <th>Sexe</th>
                        <th>Âge</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {students && students.map((student, index) => (
                        <tr key={index}>
                            <td>{index + 1}</td>
                            <td>{student.nom}</td>
                            <td>{student.prenom}</td>
                            <td>{student.matricule}</td>
                            <td>{student.sexe}</td>
                            <td>{student.age}</td>
                            <td>
                            <button onClick={() => handleEditClick(student)}>Modifier</button>
                                <DeleteBtn id={student.id}></DeleteBtn> 
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
            <div className="infos">
                <span className='infos'>Vous avez {students.length} étudiant(s)</span>
            </div>
            {selectedStudent && (
        <ModifModal student={selectedStudent} />
      )}
        </div>
    );
}

export default StudentsList;
