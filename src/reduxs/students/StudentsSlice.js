import { createSlice } from '@reduxjs/toolkit';

const initialState = []

export const StudentsSlice = createSlice({
  name: 'student',
  initialState,
  reducers: {
    addStudent: (state, action) => {
      state.push(action.payload);
    },

    deleteStudent: (state, action) => {
      const index = state.findIndex((student) => student.id === action.payload);
      if (index !== -1) {
        state.splice(index, 1);
      }
  },

  updateStudent: (state, action) => {
      const { id, nom, prenom, matricule, sexe, age } = action.payload;
      const newStudent = state.find(item => item.id === id);
        if (newStudent) {
          newStudent.nom = nom;
          newStudent.prenom = prenom;
          newStudent.matricule = matricule;
          newStudent.age = age;
          newStudent.sexe = sexe;
        }
      }
    }
})

export const { addStudent, deleteStudent, updateStudent} = StudentsSlice.actions;